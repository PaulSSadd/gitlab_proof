# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:c3254bfaa1a2a9fb583d755dcb748d9250eb05ef]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
